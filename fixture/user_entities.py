# -*- coding: utf-8 -*-


class EntitiesHelper:
    def __init__(self, app):
        self.app = app

    def search(self, search_path, xauth_key="", http_method="get",
               end_of_url="search", custom_headers={}, query={}):
        rq = self.app.requests
        url = "https://api.evotor.ru/api/v1/inventories/" + search_path + "/" + end_of_url
        headers = {}

        if xauth_key:
            headers['x-authorization'] = xauth_key
        if custom_headers:
            headers.update(custom_headers)

        response = rq.request(http_method.upper(), url, headers=headers, params=query)
        return response
