# -*- coding: utf-8 -*-

import requests
import json
from fixture.user_entities import EntitiesHelper
from fixture.products import ProductsHelper
from fixture.documents import DocumentsHelper


class Application:

    def __init__(self):
        self.requests = requests
        self.json = json

        self.xauth_keys = dict()
        self.xauth_keys['default'] = "44c03931-bcd4-4d7f-a61d-f031b5b3aac3"

        self.stores = dict()
        self.stores['default'] = "20171010-2DC3-402D-8003-FF176DAFBD0F"
        self.stores['stas'] = "20171009-4924-40B7-80F4-0AB2A98B5CD5"

        self.defaults = dict()
        self.defaults['uuid1'] = "a6386f48-be44-11e7-abc4-cec278b6b50a"

        self.user_entities = EntitiesHelper(self)
        self.products = ProductsHelper(self)
        self.documents = DocumentsHelper(self)

    @staticmethod
    def rand_uuid(version):
        if version == 1:
            uuid_url = "https://www.uuidgenerator.net/api/version4/1"
        elif version == 4:
            uuid_url = "https://www.uuidgenerator.net/api/version1/1"
        else:
            return None
        uuid_response = requests.request("GET", uuid_url)
        return uuid_response.text

    @staticmethod
    def ipsum_string(length):
        text = "But I must explain to you how all this mistaken idea" \
               " of denouncing pleasure and praising pain was born and" \
               " I will give you a complete account of the system, and" \
               " expound the actual teachings of the great explorer of " \
               "the truth, the master-builder of human happiness. No one" \
               " rejects, dislikes, or avoids pleasure itself, because it" \
               " is pleasure, but because those who do not know how to" \
               " pursue pleasure rationally encounter consequences that" \
               " are extremely painful. Nor again is there anyone who loves" \
               " or pursues or desires to obtain pain of itself, because it" \
               " is pain, but because occasionally circumstances occur in which" \
               " toil and pain can procure him some great pleasure. To take a" \
               " trivial example, which of us ever undertakes laborious physical" \
               " exercise, except to obtain some advantage from it? But who has " \
               "any right to find fault with a man who chooses to enjoy a pleasure " \
               "that has no annoying consequences, or one who avoids a pain that " \
               "produces no resultant pleasure?"
        remove_char_cnt = len(text) - length
        text = text[:-remove_char_cnt]
        return text


    # def destroy(self):
        # пока не нужен (для себя оставил, чтобы не забыть)
