# -*- coding: utf-8 -*-


class ProductsHelper:
    def __init__(self, app):
        self.app = app

    def search(self, http_method="get", store_uuid="", xauth_key="",
               custom_headers={}, query={}):
        if not store_uuid:
            store_uuid = self.app.stores.get('default')
        rq = self.app.requests
        url = "https://api.evotor.ru/api/v1/inventories/stores/" + store_uuid + "/products"
        headers = {}

        if xauth_key:
            headers['x-authorization'] = xauth_key
        if custom_headers:
            headers.update(custom_headers)

        response = rq.request(http_method.upper(), url, headers=headers, params=query)
        return response

    def delete(self, http_method="post", store_uuid="",
               xauth_key="", product_uuid="",
               custom_headers={}, query={}):
        rq = self.app.requests
        headers = dict()
        headers['content-type'] = "application/json"
        if not store_uuid:
            store_uuid = self.app.stores.get('default')
        if xauth_key:
            headers['x-authorization'] = xauth_key
        if custom_headers:
            headers.update(custom_headers)
        url = "https://api.evotor.ru/api/v1/inventories/stores/" + store_uuid + "/products/delete"

        if product_uuid:
            payload = "[{\"uuid\": " + product_uuid + "}]"
            response = rq.request(http_method.upper(), url, data=payload, headers=headers, params=query)
        else:
            response = rq.request(http_method.upper(), url, headers=headers, params=query)
        return response

    def add(self, product, http_method="post", store_uuid="", xauth_key="",
            custom_headers={}, query={}):
        rq = self.app.requests
        headers = dict()
        headers['content-type'] = 'application/json'
        if not store_uuid:
            store_uuid = self.app.stores.get('default')
        if xauth_key:
            headers['x-authorization'] = xauth_key
        if custom_headers:
            headers.update(custom_headers)
        url = "https://api.evotor.ru/api/v1/inventories/stores/" + store_uuid + "/products/"
        response = rq.request(http_method.upper(), url, data=product, headers=headers, params=query)
        return response
