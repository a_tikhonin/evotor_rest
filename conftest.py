# -*- coding: utf-8 -*-

import pytest
from fixture.application import Application


@pytest.fixture(scope="session")
def app():
    fixture = Application()
    # todo def app(request) -> request.addfinalizer(fixture.destroy)
    return fixture
