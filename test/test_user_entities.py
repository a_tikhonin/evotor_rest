# -*- coding: utf-8 -*-
# Сущности пользователя Эвотор

import pytest


@pytest.mark.parametrize("search_path", ["stores", "devices", "employees"])
def test_entities_search_pos_with_authkey(app, search_path):
    response = app.user_entities.search(search_path=search_path,
                                        xauth_key=app.xauth_keys.get('default'))
    assert response.status_code == 200


@pytest.mark.parametrize("search_path", ["stores", "devices", "employees"])
@pytest.mark.parametrize("http_method", ["post", "put", "patch", "delete"])
def test_entities_search_crud(app, search_path, http_method):
    response = app.user_entities.search(http_method=http_method,
                                        search_path=search_path,
                                        xauth_key=app.xauth_keys.get('default'))
    assert response.status_code == 405


@pytest.mark.parametrize("search_path", ["stores", "devices", "employees"])
def test_entities_search_neg_with_args(app, search_path):
    response = app.user_entities.search(xauth_key=app.xauth_keys.get('default'),
                                        search_path=search_path,
                                        query={'a': 2, 'b': '4<>}'})
    assert response.status_code == 200


@pytest.mark.parametrize("search_path", ["stores", "devices", "employees"])
@pytest.mark.parametrize("end_of_url", ["999999999999999", "\'abc\'"])
def test_entities_search_neg_non_existent_page(app, search_path, end_of_url):
    response = app.user_entities.search(xauth_key=app.xauth_keys.get('default'),
                                        search_path=search_path,
                                        end_of_url=end_of_url)
    assert response.status_code == 404


@pytest.mark.parametrize("search_path", ["stores", "devices", "employees"])
def test_entities_search_neg_without_xauth_key(app, search_path):
    response = app.user_entities.search(search_path=search_path)
    assert response.status_code == 401


@pytest.mark.parametrize("search_path", ["stores", "devices", "employees"])
def test_entities_search_neg_wrong_accept(app, search_path):
    response = app.user_entities.search(xauth_key=app.xauth_keys.get('default'),
                                        search_path=search_path,
                                        custom_headers={'accept': 'application/xml'})
    assert response.status_code == 406
