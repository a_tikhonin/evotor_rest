# -*- coding: utf-8 -*-
# Работа с товарами в облаке Эвотор

import pytest
from model.product import Product


# Получить список товаров
def test_products_search_pos_with_authkey(app):
    response = app.products.search(xauth_key=app.xauth_keys.get('default'))
    assert response.status_code == 200


@pytest.mark.parametrize("http_method", ["delete"])
def test_products_search_crud(app, http_method):
    response = app.products.search(http_method=http_method,
                                   xauth_key=app.xauth_keys.get('default'))
    assert response.status_code == 405


def test_products_search_neg_unavailable_store(app):
    response = app.products.search(store_uuid="20170222-5A12-403A-80F6-75C361356826",
                                   xauth_key=app.xauth_keys.get('default'))
    assert response.status_code == 401


def test_products_search_neg_non_existent_page(app):
    response = app.products.search(store_uuid="<'@&#$*)(#%^",
                                   xauth_key=app.xauth_keys.get('default'))
    assert response.status_code == 404


def test_products_search_neg_without_authkey(app):
    response = app.products.search()
    assert response.status_code == 401


def test_products_search_neg_wrong_accept(app):
    response = app.products.search(xauth_key=app.xauth_keys.get('default'),
                                   custom_headers={'accept': 'application/xml'})
    assert response.status_code == 406


# Удалить товар / все товары
def test_products_delete_pos_clear_all(app):
    delete_response = app.products.delete(xauth_key=app.xauth_keys.get('default'))
    assert delete_response.status_code == 200
    check_response = app.products.search(xauth_key=app.xauth_keys.get('default'))
    assert "[]" == check_response.text


@pytest.mark.parametrize("http_method", ["get", "put", "patch", "delete"])
def test_products_delete_crud(app, http_method):
    response = app.products.delete(xauth_key=app.xauth_keys.get('default'),
                                   http_method=http_method)
    assert response.status_code == 405


@pytest.mark.parametrize("product_uuid", ["\"2e604208-ba56-11e7-abc4-cec278b6b50a\"", "\"\"", "1", "null", "false"])
def test_products_delete_neg_wrong_uuid(app, product_uuid):
    response = app.products.delete(xauth_key=app.xauth_keys.get('default'),
                                   product_uuid=product_uuid)
    assert response.status_code == 400


def test_products_delete_neg_unavailable_store(app):
    response = app.products.delete(xauth_key=app.xauth_keys.get('default'),
                                   store_uuid="20171010-2DC3-402D-8003-FF176DAFBD0")
    assert response.status_code == 401


def test_products_delete_neg_without_authkey(app):
    response = app.products.delete()
    assert response.status_code == 401


def test_products_delete_neg_wrong_contenttype(app):
    response = app.products.delete(xauth_key=app.xauth_keys.get('default'),
                                   custom_headers={'content-type': 'application/xml'})
    assert response.status_code == 415


# Добавить товар
def test_products_add_pos(app):
    test_product = Product(app)
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


@pytest.mark.parametrize("uuid", [None, "", "a6386f48-be44-11e7-abc4-cec278b6b50a", False, 1.2345, 12345])
def test_products_add_neg_uuid(app, uuid):
    test_product = Product(app, update={'uuid': uuid})
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


def test_products_add_neg_uuid_longstring(app):
    test_product = Product(app, update={'uuid': app.ipsum_string(101)})
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


def test_products_add_neg_without_uuid(app):
    test_product = Product(app, without=['uuid'])
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


@pytest.mark.parametrize("group, quantity", [(False, None), (False, ""), (False, -10),
                                             (False, 0.00001), (False, 2.99792458e8), (True, 2)])
def test_products_add_neg_gr_quantity(app, group, quantity):
    test_product = Product(app, is_group=group, update={'quantity': quantity})
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


@pytest.mark.parametrize("name", ["", None, -1, False, 1.234])
def test_products_add_neg_name(app, name):
    test_product = Product(app, update={'name': name})
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


def test_products_add_neg_name_longstring(app):
    test_product = Product(app, update={'name': app.ipsum_string(101)})
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


@pytest.mark.parametrize("group", [None, 1, "true", "NORMAL"])
def test_products_add_neg_group(app, group):
    test_product = Product(app, is_group=True, update={'group': group})
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response


def test_products_add_neg_without_group(app):
    test_product = Product(app, is_group=True, without=['group'])
    add_response = app.products.add(xauth_key=app.xauth_keys.get('default'),
                                    product=test_product.json)
    # check_response
    # delete_response
