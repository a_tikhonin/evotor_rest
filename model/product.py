class Product:
    def __init__(self, app,
                 is_group=False,
                 is_alcohol=False,
                 hasVariants=None,
                 costPrice=None,
                 description=None,
                 articleNumber=None,
                 code=None,
                 barCodes=None,
                 attributes=None,
                 update={},
                 without=[]):

        self.app = app
        self.data = dict()

        self.group = {
            "uuid": self.app.rand_uuid(version=4),
            "name": "test_group",
            "group": True,
            "parentUuid": None
        }

        self.product = {
            "uuid": self.app.rand_uuid(version=4),
            "name": "test_product",
            "group": False,
            "type": "NORMAL",
            "quantity": 10,
            "measureName": "шт",
            "tax": "NO_VAT",
            "price": 900,
            "allowToSell": True,
            "parentUuid": None
        }

        self.alcohol = {
            "name": "test_alcohol",
            "type": "ALCOHOL_NOT_MARKED",
            "alcoholByVolume": 5.45,
            "alcoholProductKindCode": 123,
            "tareVolume": 0.57,
            "alcoCodes": ["0000000000000000001"]
        }

        self.not_required = dict()
        if hasVariants is not None:
            self.not_required["hasVariants"] = hasVariants
        if costPrice is not None:
            self.not_required["costPrice"] = costPrice
        if description is not None:
            self.not_required["description"] = description
        if articleNumber is not None:
            self.not_required["articleNumber"] = articleNumber
        if code is not None:
            self.not_required["code"] = code
        if barCodes is not None:
            self.not_required["barCodes"] = barCodes
        if attributes is not None:
            self.not_required["attributes"] = attributes

        if is_group:
            self.data.update(self.group)
        else:
            self.data.update(self.product)
        if is_alcohol:
            self.data.update(self.alcohol)
        self.data.update(self.not_required)
        self.data.update(update)

        for field in without:
            del self.data[field]

        self.data = [self.data]
        self.json = self.app.json.dumps(self.data)
